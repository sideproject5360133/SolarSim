//
// Created by argo on 24/08/17.
//

#include "Light.h"

int Light::numberLight(){
    return 8;
}

GLenum Light::getLight(int i){
    switch(i){
        case 0: return GL_LIGHT0;
        case 1: return GL_LIGHT1;
        case 2: return GL_LIGHT2;
        case 3: return GL_LIGHT3;
        case 4: return GL_LIGHT4;
        case 5: return GL_LIGHT5;
        case 6: return GL_LIGHT6;
        default: return GL_LIGHT7;
    }
}

int Light::getNumber(GLenum i){
    switch(i){
        case GL_LIGHT0: return 0;
        case GL_LIGHT1: return 1;
        case GL_LIGHT2: return 2;
        case GL_LIGHT3: return 3;
        case GL_LIGHT4: return 4;
        case GL_LIGHT5: return 5;
        case GL_LIGHT6: return 6;
        default: return 7;
    }
}