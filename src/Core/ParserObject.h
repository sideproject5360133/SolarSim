//
// Created by argo on 30/08/17.
//

#ifndef SOLARSIM_PARSEROBJECT_H
#define SOLARSIM_PARSEROBJECT_H

#include <src/Engine/Renderer/RenderObject.h>
#include <src/Engine/ManagerTexture.h>
#include <src/Engine/Renderer/ManagerRenderer.h>

class ParserObject{

public:
    static bool next;
    static ManagerTexture *managerTexture;
    static ManagerRenderer *managerRenderer;
    static void openFile(string id);
    static void addManagerTexture(ManagerTexture *managerTexture);
    static void addManagerRenderer(ManagerRenderer *managerRenderer);

    static void getNextPlanet();
    static int hasNextPlanet();
    static void addFileToRender(string id);

};

#endif //SOLARSIM_PARSEROBJECT_H
