//
// Created by argo on 14/08/17.
//

/******************************************************
 * Static class to read a directory
 *
 * NB:
 * Class has an 'iterator', to read the directory file
 * per file easily.
 ******************************************************/
#ifndef SOLARSIM_READDIRECTORY_H
#define SOLARSIM_READDIRECTORY_H

#include <string>
#include <dirent.h>
using namespace std;

class ReadDirectory{
public:
    static DIR * rep; //Directory
    static struct dirent * ent; //Content of a file

    /* Function nameWithoutExtension
     * Return 's' without the extension */
    static string nameWithoutExtension(string s);
    /* Function isFile
     * return 1 if it's a file, 0 else */
    static int isFile(string * s);
    static bool fileExist(string dir,string file);

    /****** Iterator *****/
    static void iteratorFirst(string id); //Open the directory named 'id'
    static int iteratorHasNext(); //Iterator has a next file
    static struct dirent * iteratorNext(); //Return the current file and itr going to next
    static string iteratorNextS(); //Return the NAME of the current file and itr going to next
    static bool iteratorNextS(string *s); //Return the NAME of the current file and itr going to next
};

#endif //SOLARSIM_READDIRECTORY_H
