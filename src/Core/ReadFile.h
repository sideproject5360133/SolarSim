//
// Created by argo on 30/08/17.
//

#ifndef SOLARSIM_READFILE_H
#define SOLARSIM_READFILE_H

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class ReadFile {
public:
    static ifstream * file;
    static ofstream * fileW;

    static void openFile(string s);
    static void writeLine(string s);

    /****** Iterator *****/
    static void iteratorFirst(string id); //Open the directory named 'id'
    static int iteratorHasNextLine(); //Iterator has a next file
    static string iteratorNextLine();
    static string iteratorNextString();
    static float iteratorNextFloat();
    static int iteratorNextInt();

};

#endif //SOLARSIM_READFILE_H
