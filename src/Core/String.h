//
// Created by argo on 16/08/17.
//

/******************************************************
 * Static class to manage easily String
 ******************************************************/

#ifndef SOLARSIM_STRING_H
#define SOLARSIM_STRING_H

#include <string>
using namespace std;

class String{
public:

    enum Color{RED,GREEN}; //Color for string

    /* Function replacePiece
     * Return a string construct from 's', where
     * if we find 'one', it's replaced by 'two' */
    static string replacePiece(string s,string one,string two);

    /* Function colorize
     * Return 's' colorized with 'color' */
    static string colorize(string s,Color color);
};

#endif //SOLARSIM_STRING_H
