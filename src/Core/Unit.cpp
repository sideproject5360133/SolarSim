//
// Created by argo on 21/08/17.
//

#include <src/Debug/Log.h>
#include "Unit.h"

struct Unit::Distance Unit::size={val: 1,dist: UA};
struct Unit::Distance Unit::high={val: 1,dist: UA};
struct Unit::Time Unit::speed={val: 1, time: SEC};
struct Unit::Time Unit::speedA={val: 1, time: SEC};
/*
Unit::Unit_Term Unit::size=Unit::UA;
Unit::Unit_Term Unit::high=Unit::UA;
Unit::Unit_Term Unit::speed=Unit::UA;
int Unit::sizeI=1;
int Unit::highI=1;
int Unit::speedI=1;*/

float Unit::getKmFromUnit(Unit_Term term){
    switch(term){
        case UA: return 150000000;
        case Km: return 1;
        case TKm: return 1000;
        case MKm: return 1000000;
        case BKm: return 1000000000;
    }
}

float Unit::getUAFromUnit(Unit_Term term){
    switch(term){
        case UA: return 1;
        case Km: return 1/150000000;
        case TKm: return 1/150000;
        case MKm: return 1/150;
        case BKm: return 1/0.15;
    }
}

float Unit::getValUnit(Distance dist){
    switch(dist.dist){
        case UA: return UNIT_BASE*dist.val;
        case Km: return dist.val/1000;
        case TKm: return dist.val;
        case MKm: return 1000*dist.val;
        case BKm: return 1000000*dist.val;
    }
}

float Unit::getValUnit2(Time time){
    switch(time.time){
        case SEC: return time.val;
        case MIN: return 60*time.val;
        case HOU: return 60*60*time.val;
        case DAY: return 60*60*24*time.val;
        case MON: return 60*60*24*30*time.val;
        case YEA: return 60*60*24*360*time.val;
    }
}

float Unit::getSizeUnit(){
    Log::floatValue("val",size.val);
    return getValUnit(size);
}

float Unit::getHighUnit(){
    return getValUnit(high);
}

float Unit::getSpeedUnit(){
    return getValUnit2(speed);
}

float Unit::getSpeedAUnit(){
    return getValUnit2(speedA);
}