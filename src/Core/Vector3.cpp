//
// Created by argo on 15/06/17.
//

#include <glm/glm.hpp>
#include "Vector3.h"

float Vector3::length(glm::vec3 vector){
    float x = vector.x;
    float y = vector.y;
    float z = vector.z;

    return glm::sqrt(x*x+y*y+z*z);
}

float Vector3::dotProduct(glm::vec3 vec1, glm::vec3 vec2){
    return (float) glm::dot(vec1,vec2);
}

glm::vec3 Vector3::crossProductFloat(glm::vec3 vec1, glm::vec3 vec2){
    return glm::cross(vec1,vec2);
}