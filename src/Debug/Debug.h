//
// Created by argo on 13/06/17.
//

/******************************************************
 * Static class to do some debug on launching
 ******************************************************/

#ifndef SOLARSIM_DEBUG_H
#define SOLARSIM_DEBUG_H

class Debug{
public:
    static void verificationStart(); // "Main debug", have to be launched with the program

    static int testListRenderer(); //Test for ListRenderer
    static int testList(); //Test for generic List
    static void testReadDirectory(); //Test for ReadDirectory
    static void testReadFile(); //Test for ReadFile
    static int testString(); //Test for String

};

#endif //SOLARSIM_DEBUG_H
