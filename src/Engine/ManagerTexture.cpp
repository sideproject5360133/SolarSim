//
// Created by argo on 11/08/17.
//

#include <QtGui/QImage>
#include <QtOpenGL/QGLWidget>
#include "ManagerTexture.h"



void ManagerTexture::addTexture(string name,string path){
    GLuint texture;
    loadTexture(QString::fromStdString("../textures/"+path),&texture);
    list.add(new Texture(name,texture));
}

void ManagerTexture::removeTexture(string name){
    Texture * texture = getObjectTextureFromId(name);
    if(texture!= nullptr) list.erase(texture);
}

void ManagerTexture::loadTexture(QString textureName, GLuint *texture) {
    QImage qim_Texture;
    QImage qim_TempTexture;
    qim_TempTexture.load(textureName);
    qim_Texture = QGLWidget::convertToGLFormat( qim_TempTexture );
    glGenTextures( 1, texture );
    glBindTexture( GL_TEXTURE_2D, *texture );
    glTexImage2D( GL_TEXTURE_2D, 0, 3, qim_Texture.width(), qim_Texture.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, qim_Texture.bits() );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
}

Texture * ManagerTexture::getObjectTextureFromId(string id){
    list.iteratorFirst();
    while(list.iteratorHasNext()){
        Texture * texture=list.iteratorNext();
        if(texture->id==id) return texture;
    }
    return nullptr;
}

GLuint ManagerTexture::getTextureFromId(string id){
    list.iteratorFirst();
    while(list.iteratorHasNext()){
        Texture * texture=list.iteratorNext();
        if(texture->id==id) return texture->texture;
    }
    return GLuint();
}

List<string> ManagerTexture::getListID(){
    List<string> l;
    list.iteratorFirst();
    while(list.iteratorHasNext()){
        Texture * texture=list.iteratorNext();
        l.add(texture->id);
    }
    return l;
}

void ManagerTexture::debugFunction(){
    list.iteratorFirst();
    Log::print("\n");
    Log::debug("Debug Function ManagerTexture:");
    while(list.iteratorHasNext()){
        Texture *texture=list.iteratorNext();
        Log::debug("-> "+texture->id);
    }
    Log::debug("End Debug Function ManagerTexture\n");
}