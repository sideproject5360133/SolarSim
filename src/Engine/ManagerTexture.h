//
// Created by argo on 11/08/17.
//

/******************************************************
 * Class to Manage Texture
 ******************************************************/

#ifndef SOLARSIM_MANAGERTEXTURE_H
#define SOLARSIM_MANAGERTEXTURE_H

#include <src/Core/List.h>
#include <QtCore/QString>
#include <src/Engine/Renderer/RenderObject.h>
#include "Texture.h"

class ManagerTexture{
public:
    List<Texture*> list;

    void addTexture(string name,string path);
    void removeTexture(string name);


    void loadTexture(QString textureName, GLuint *texture);

    Texture * getObjectTextureFromId(string id);
    GLuint getTextureFromId(string id);
    List<string> getListID();

    void debugFunction();

    };

#endif //SOLARSIM_MANAGERTEXTURE_H
