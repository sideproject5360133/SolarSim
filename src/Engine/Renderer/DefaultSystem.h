//
// Created by argo on 30/08/17.
//

#ifndef SOLARSIM_DEFAULTSYSTEM_H
#define SOLARSIM_DEFAULTSYSTEM_H

#include <src/Engine/ManagerTexture.h>
#include <QtWidgets/QListWidget>
#include "ManagerRenderer.h"

class DefaultSystem {
public:

    explicit DefaultSystem(ManagerRenderer *managerRenderer, ManagerTexture *managerTexture);

    void debug();
    void solarSystemIllustration();
    void earthIllustration();
    void earthReal();

    void fullfill(QListWidget *list);
    void fullfillUser(QListWidget *list);
    void charge(string s);

private:
    ManagerRenderer *managerRenderer;
    ManagerTexture *managerTexture;
};

#endif //SOLARSIM_DEFAULTSYSTEM_H
