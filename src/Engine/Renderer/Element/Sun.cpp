//
// Created by argo on 21/07/17.
//

#include <src/Debug/Log.h>
#include <src/Gui/GLWindow.h>
#include <GL/glu.h>
#include <src/Core/Light.h>
#include "Sun.h"
#include "src/Engine/Renderer/RenderObject.h"

Sun::Sun(int i, std::string id, Texture * texture,
         string size, int unitSize,
         string high, int unitHigh,
         string speed, int unitSpeed,
         string axis,
         string speedA, int unitSpeedA) : RenderObject(RenderObject::Sun,id,texture){

    Log::debug("Construct Sun");
    Log::intValue("nSun",i);
    num_light=i;
    light=Light::getLight(i);
    GLfloat blanc[4]={1.5,1.5,1.5,1};
    GLenum light=Light::getLight(i);
    glEnable(light);
    glLightfv(light, GL_AMBIENT,blanc);
    glLightfv(light, GL_DIFFUSE,blanc);
    glLightfv(light, GL_SPECULAR,blanc);
    position[3]=1;

    changeSize(size,unitSize);
    changeHigh(high,unitHigh);
    changeSpeed(speed,unitSpeed);
    changeAxisTilt(axis);
    changeSpeedAxis(speedA,unitSpeedA);
}

void Sun::render(){
    glm::vec3 tran = GLWindow::getLookPlanet();
    GLfloat newPos[4];
    newPos[0]=position[0]-tran.x;
    newPos[1]=position[1]-tran.y;
    newPos[2]=position[2]-tran.z;
    newPos[3]=position[3];
    glLightfv(light, GL_POSITION, newPos);
    GLUquadric * params = gluNewQuadric();
    gluQuadricTexture(params, GL_TRUE);
    glBindTexture(GL_TEXTURE_2D, texture->texture);

    glPushMatrix();
    glTranslatef(position[0],position[1],position[2]);
    glRotatef(valueAxis,0,0,1);
    glRotatef(axis,1,0,0);

    gluSphere(params, size, 20, 20);

    glPopMatrix();
    //gluSphere(params, size, 20, 20);
    //glTranslatef(newPos[0], newPos[1], newPos[2]);


    gluDeleteQuadric(params);

}