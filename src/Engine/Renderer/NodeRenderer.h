//
// Created by argo on 12/06/17.
//

/******************************************************
 * NodeRenderer, node of a ListRenderer
 * Contain the RenderObject and nextNode
 ******************************************************/

#ifndef SOLARSIM_NODERENDERER_H
#define SOLARSIM_NODERENDERER_H

#include "RenderObject.h"

class NodeRenderer {
public:

    //Attribute
    NodeRenderer * nextNode;
    RenderObject * renderObject;

    //Constructor
    explicit NodeRenderer(RenderObject * renderObject = nullptr, NodeRenderer * nextNode = nullptr);

    void addRenderObject(RenderObject * renderObject);
    RenderObject * getRenderObject();

    void addNext(NodeRenderer * nextNode);
    NodeRenderer * getNextNode();
};

#endif //SOLARSIM_NODERENDERER_H
