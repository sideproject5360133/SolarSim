//
// Created by argo on 11/08/17.
//

#include "Texture.h"

Texture::Texture(string id, GLuint texture){
    this->id=id;
    this->texture=texture;
}

int Texture::textureEqual(Texture *texture){
    if(this->id==texture->id) return 1;
    return 0;
}