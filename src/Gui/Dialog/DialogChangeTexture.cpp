//
// Created by argo on 11/08/17.
//

#include "ui_DialogChangeTexture.h"
#include "DialogChangeTexture.h"
#include "src/Debug/Log.h"
#include "DialogChargeNewTexture.h"
#include <QtOpenGL/QGLFormat>
#include <QPushButton>

DialogChangeTexture::DialogChangeTexture(GLWindow *glWindow, QWidget *parent) :
        QDialog(parent),
        ui(new Ui::DialogChangeTexture) {
    ui->setupUi(this);
    this->glWindow = glWindow;
    this->setWindowTitle("Choose a model to change his texture");
    this->setFixedSize(this->size());

    okButton = ui->buttonBox->button(QDialogButtonBox::Apply);
    cancelButton = ui->buttonBox->button(QDialogButtonBox::Cancel);
    resetButton = ui->buttonBox->button(QDialogButtonBox::Reset);
    okButton->setText(QString::fromStdString("Apply"));
    resetButton->setText(QString::fromStdString("Model"));
    cancelButton->setText(QString::fromStdString("Close"));
    okButton->setEnabled(false);
    resetButton->setEnabled(false);
    ui->listWidget_2->setVisible(false);
    ui->pushButton->setVisible(false);
    refreshListModel();
    mode = 1;


    connect(okButton, SIGNAL(released()), this, SLOT (handleOkButton()));
    connect(resetButton, SIGNAL(released()), this, SLOT (handleResetButton()));
    connect(ui->listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem * )), this,
            SLOT(handleDoubleClickItem(QListWidgetItem * )));
    connect(ui->listWidget, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem(QListWidgetItem * )));
    connect(ui->listWidget_2, SIGNAL(itemDoubleClicked(QListWidgetItem * )), this,
            SLOT(handleDoubleClickItem2(QListWidgetItem * )));
    connect(ui->listWidget_2, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem2(QListWidgetItem * )));
    connect(ui->pushButton, SIGNAL(released()), this, SLOT(handleChargeNew()));
}

void DialogChangeTexture::refreshListModel() {
    ui->listWidget->clear();
    ListRenderer *list = glWindow->getManagerRenderer()->getAllList();
    list->iteratorFirst();
    QString id;
    while (list->iteratorHasNext()) {
        id = QString::fromStdString(list->iteratorNext()->id);
        ui->listWidget->addItem(new QListWidgetItem(id));
    }
}

void DialogChangeTexture::refreshListTexture() {
    ui->listWidget_2->clear();
    List<string> list = glWindow->getManagerTexture()->getListID();
    list.iteratorFirst();
    QString id;
    while (list.iteratorHasNext()) {
        id = QString::fromStdString(list.iteratorNext());
        ui->listWidget_2->addItem(new QListWidgetItem(id));
    }
}

void DialogChangeTexture::handleOkButton() {
    if (mode == 2){
        ro->texture=textureToApply;
    }else {
        ui->listWidget->setVisible(false);
        resetButton->setEnabled(true);
        okButton->setEnabled(false);
        this->setWindowTitle("Choose a texture to apply");
        refreshListTexture();
        ui->listWidget_2->setVisible(true);
        ui->pushButton->setVisible(true);
        mode = 2;
    }
}

void DialogChangeTexture::handleResetButton() {
    resetButton->setEnabled(false);
    refreshListModel();
    ui->listWidget->setVisible(true);
    okButton->setEnabled(false);
    ui->listWidget_2->setVisible(false);
    ui->pushButton->setVisible(false);
    mode=1;
    this->setWindowTitle("Choose a model to change his texture");
}

void DialogChangeTexture::handleChargeNew() {
    DialogChargeNewTexture dialog(glWindow);
    dialog.exec();
    refreshListTexture();
}

void DialogChangeTexture::getTextureFromList(string id) {
    ListRenderer *list = glWindow->getManagerRenderer()->getAllList();
    list->iteratorFirst();
    QString id2;
    while (list->iteratorHasNext()) {
        RenderObject *ro2 = list->iteratorNext();
        if (ro2->id == id){
            ro = ro2;
        }
    }
}

void DialogChangeTexture::getTexture2FromList(string id) {
    List<Texture*> list = glWindow->getManagerTexture()->list;
    list.iteratorFirst();
    QString id2;
    while (list.iteratorHasNext()) {
        Texture *texture = list.iteratorNext();
        if (texture->id == id) textureToApply = texture;
    }
}

void DialogChangeTexture::handleActivatedItem(QListWidgetItem *item) {
    Log::debug("Item activated: " + item->text().toStdString());
    getTextureFromList(item->text().toStdString());
    glWindow->setLookPlanetId(item->text().toStdString());
    okButton->setEnabled(true);
}

void DialogChangeTexture::handleDoubleClickItem(QListWidgetItem *item) {
    Log::debug("Item double click: " + item->text().toStdString());
    getTextureFromList(item->text().toStdString());
    glWindow->setLookPlanetId(item->text().toStdString());
    handleOkButton();

}

void DialogChangeTexture::handleActivatedItem2(QListWidgetItem *item) {
    Log::debug("Item activated: " + item->text().toStdString());
    getTexture2FromList(item->text().toStdString());
    okButton->setEnabled(true);
}

void DialogChangeTexture::handleDoubleClickItem2(QListWidgetItem *item) {
    Log::debug("Item double click: " + item->text().toStdString());
    handleOkButton();

}

DialogChangeTexture::~DialogChangeTexture() {
    delete ui;
}
