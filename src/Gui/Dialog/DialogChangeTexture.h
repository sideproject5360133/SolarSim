//
// Created by argo on 11/08/17.
//

#ifndef SOLARSIM_DIALOGCHANGETEXTURE_H
#define SOLARSIM_DIALOGCHANGETEXTURE_H

#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <src/Gui/GLWindow.h>
#include <QListWidgetItem>

namespace Ui {
    class DialogChangeTexture;
}

class DialogChangeTexture : public QDialog
{
    Q_OBJECT

public:
    explicit DialogChangeTexture(GLWindow * glWindow,QWidget *parent = 0);

    void refreshListModel();
    void refreshListTexture();

    void getTextureFromList(string id);
    void getTexture2FromList(string id);

        ~DialogChangeTexture();

private:
    //QDialogButtonBox *ui;
    Ui::DialogChangeTexture *ui;
    GLWindow * glWindow;
    QPushButton* okButton,*cancelButton,*resetButton;
    RenderObject * ro;
    Texture *textureToApply;
    int mode;


protected slots:
    void handleOkButton();
    void handleResetButton();
    void handleChargeNew();
    void handleActivatedItem(QListWidgetItem *item);
    void handleDoubleClickItem(QListWidgetItem *item);
    void handleActivatedItem2(QListWidgetItem *item);
    void handleDoubleClickItem2(QListWidgetItem *item);


};

#endif //SOLARSIM_DIALOGCHANGETEXTURE_H
