//
// Created by argo on 14/08/17.
//

#ifndef SOLARSIM_DIALOGCHARGENEWTEXTURE_H
#define SOLARSIM_DIALOGCHARGENEWTEXTURE_H

#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <src/Gui/GLWindow.h>
#include <QListWidgetItem>

namespace Ui {
    class DialogChargeNewTexture;
}

class DialogChargeNewTexture : public QDialog
{
    Q_OBJECT

public:
    explicit DialogChargeNewTexture(GLWindow * glWindow,QWidget *parent = 0);

    ~DialogChargeNewTexture();

private:
    Ui::DialogChargeNewTexture *ui;

    GLWindow * glWindow;

    QPushButton* cancelButton,*refresh,*remove,*removeDef,*tree;



protected slots:


    void handleRefresh();
    void handleTree();
    void handleRemove();
    void handleRemoveDef();


};

#endif //SOLARSIM_DIALOGCHARGENEWTEXTURE_H
