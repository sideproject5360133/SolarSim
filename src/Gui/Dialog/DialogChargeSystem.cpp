//
// Created by argo on 29/08/17.
//


#include "ui_DialogChargeSystem.h"
#include "DialogChargeSystem.h"
#include "src/Debug/Log.h"
#include <QtOpenGL/QGLFormat>
#include <QPushButton>
#include <sys/socket.h>
#include <src/Core/String.h>
#include <src/Core/ParserObject.h>

DialogChargeSystem::DialogChargeSystem(MainWindow *mainWindow,QWidget *parent) :
        QDialog(parent),
        ui(new Ui::DialogChargeSystem) {
    ui->setupUi(this);

    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    this->setFixedSize(this->size());
    this->setWindowTitle("Load System");

    this->mainWindow=mainWindow;
    fullfillDefault();
    fullfillUser();
    defaultSystem=new DefaultSystem(mainWindow->getManagerRenderer(),mainWindow->getManagerTexture());


    ui->pushButton_2->setEnabled(false);
    connect(ui->pushButton, SIGNAL(released()), this, SLOT (handleNew()));
    connect(ui->pushButton_2, SIGNAL(released()), this, SLOT (handleCharge()));

    connect(ui->listWidget, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem(QListWidgetItem * )));
    connect(ui->listWidget_2, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem_2(QListWidgetItem * )));
}

void DialogChargeSystem::fullfillDefault(){
    defaultSystem->fullfill(ui->listWidget);
}

void DialogChargeSystem::fullfillUser(){
    defaultSystem->fullfillUser(ui->listWidget_2);
}

void DialogChargeSystem::handleNew() {
    mainWindow->setVisible(true);
    this->close();
}

void DialogChargeSystem::handleCharge() {
    if(defaultChoice){
        defaultSystem->charge(choice->text().toStdString());
    }else{
        string file=String::replacePiece(choice->text().toStdString()," ","_");
        ParserObject::addFileToRender(file);
    }
    mainWindow->setVisible(true);
    this->close();
}

void DialogChargeSystem::handleActivatedItem(QListWidgetItem * item) {
    ui->pushButton_2->setEnabled(true);
    ui->listWidget_2->setCurrentRow(-1);
    choice=item;
    defaultChoice=true;
}

void DialogChargeSystem::handleActivatedItem_2(QListWidgetItem * item){
    ui->pushButton_2->setEnabled(true);

    ui->listWidget->setCurrentRow(-1);
    choice=item;
    defaultChoice=false;

}

DialogChargeSystem::~DialogChargeSystem() {
    delete ui;
}