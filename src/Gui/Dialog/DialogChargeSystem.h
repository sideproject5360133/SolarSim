//
// Created by argo on 29/08/17.
//

#ifndef SOLARSIM_DIALOGCHARGESYSTEM_H
#define SOLARSIM_DIALOGCHARGESYSTEM_H

#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QListWidgetItem>
#include <src/Gui/MainWindow.h>
#include <src/Engine/Renderer/DefaultSystem.h>

namespace Ui {
    class DialogChargeSystem;
}

class DialogChargeSystem : public QDialog
{
Q_OBJECT

public:
    explicit DialogChargeSystem(MainWindow *mainWindow,QWidget *parent = 0);
    ~DialogChargeSystem();


private:
    Ui::DialogChargeSystem *ui;
    MainWindow *mainWindow;
    DefaultSystem *defaultSystem;

    bool defaultChoice; QListWidgetItem *choice;

    void fullfillDefault();
    void fullfillUser();

protected slots:

    void handleNew();
    void handleCharge();
    void handleActivatedItem(QListWidgetItem * item);
    void handleActivatedItem_2(QListWidgetItem * item);

};

#endif //SOLARSIM_DIALOGCHARGESYSTEM_H
