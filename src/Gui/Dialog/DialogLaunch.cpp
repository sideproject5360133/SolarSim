//
// Created by argo on 29/08/17.
//

#include "ui_DialogLaunch.h"
#include "DialogLaunch.h"
#include "src/Debug/Log.h"
#include "DialogChargeSystem.h"
#include <QtOpenGL/QGLFormat>
#include <QPushButton>
#include <sys/socket.h>
#include <src/Core/String.h>

DialogLaunch::DialogLaunch(MainWindow *mainWindow,QWidget *parent) :
        QDialog(parent),
        ui(new Ui::DialogLaunch) {
    ui->setupUi(this);

    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    this->setFixedSize(this->size());
    this->setWindowTitle("Welcome");
    ui->label->setText(QString::fromStdString(String::colorize(ui->label->text().toStdString(),String::RED)));

    this->mainWindow=mainWindow;

    connect(ui->pushButton, SIGNAL(released()), this, SLOT (handleNew()));
    connect(ui->pushButton_2, SIGNAL(released()), this, SLOT (handleCharge()));
}

void DialogLaunch::handleNew() {
    mainWindow->setVisible(true);
    this->close();
}

void DialogLaunch::handleCharge() {
    this->close();
    DialogChargeSystem dialogChargeSystem(mainWindow);
    dialogChargeSystem.exec();
}

DialogLaunch::~DialogLaunch() {
    delete ui;
}
