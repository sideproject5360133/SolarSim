//
// Created by argo on 29/08/17.
//

#ifndef SOLARSIM_DIALOGLAUNCH_H
#define SOLARSIM_DIALOGLAUNCH_H

#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QListWidgetItem>
#include <src/Gui/MainWindow.h>

namespace Ui {
    class DialogLaunch;
}

class DialogLaunch : public QDialog
{
    Q_OBJECT

public:
    explicit DialogLaunch(MainWindow *mainWindow,QWidget *parent = 0);
    ~DialogLaunch();


private:
    Ui::DialogLaunch *ui;
    MainWindow *mainWindow;


protected slots:
    void handleNew();
    void handleCharge();

};


#endif //SOLARSIM_DIALOGLAUNCH_H
