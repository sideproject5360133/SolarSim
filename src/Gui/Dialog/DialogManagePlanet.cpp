//
// Created by argo on 15/08/17.
//

#include "ui_DialogManagePlanet.h"
#include "DialogManagePlanet.h"
#include "src/Debug/Log.h"
#include <QtOpenGL/QGLFormat>
#include <QPushButton>
#include <QtWidgets/QMessageBox>
#include <src/Core/Verification.h>
#include <src/Core/String.h>
#include <src/Interface/MessageBox.h>
#include <src/Engine/Renderer/Element/Sun.h>
#include <typeinfo>


void initLineEditFloat(QLineEdit *lineEdit,QString *text){
    lineEdit->setMaxLength(12);
    lineEdit->setText("0.00");
    *text=QString("0.00");
}

DialogManagePlanet::DialogManagePlanet(MainWindow *mainWindow,GLWindow *glWindow,QWidget *parent) :
        QDialog(parent),
        ui(new Ui::DialogManagePlanet) {
    ui->setupUi(this);
    this->setWindowTitle("Manage planets");
    this->setFixedSize(this->size());
    this->mainWindow=mainWindow;
    this->glWindow=glWindow;
    refreshListModel();
    refreshListTexture();
    ui->pushButton->setEnabled(false);
    ui->pushButton_3->setEnabled(false);
    ui->lineEdit_5->setReadOnly(true);
    initLineEditFloat(ui->lineEdit,&text_1);
    initLineEditFloat(ui->lineEdit_3,&text_3);
    initLineEditFloat(ui->lineEdit_4,&text_4);
    initLineEditFloat(ui->lineEdit_6,&text_6);
    initLineEditFloat(ui->lineEdit_7,&text_7);
    initLineEditFloat(ui->lineEdit_8,&text_8);
    initLineEditFloat(ui->lineEdit_9,&text_9);
    initLineEditFloat(ui->lineEdit_10,&text_10);
    initLineEditFloat(ui->lineEdit_11,&text_11);
    initLineEditFloat(ui->lineEdit_12,&text_12);


    connect(ui->pushButton, SIGNAL(released()), this, SLOT(handleRemoveButton()));
    connect(ui->pushButton_3, SIGNAL(released()), this, SLOT(handleModifyButton()));
    connect(ui->listWidget, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem(QListWidgetItem * )));
    connect(ui->listWidget_3, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem_3(QListWidgetItem * )));
    connect(ui->pushButton_2, SIGNAL(released()), this, SLOT(handleAddButton()));
    connect(ui->listWidget_2, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem2(QListWidgetItem * )));
    connect(ui->listWidget_4, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem3(QListWidgetItem * )));
    connect(ui->lineEdit, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_1(const QString &)));
    connect(ui->lineEdit_3, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_3(const QString &)));
    connect(ui->lineEdit_4, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_4(const QString &)));
    connect(ui->lineEdit_6, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_6(const QString &)));
    connect(ui->lineEdit_7, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_7(const QString &)));
    connect(ui->lineEdit_8, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_8(const QString &)));
    connect(ui->lineEdit_9, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_9(const QString &)));
    connect(ui->lineEdit_10, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_10(const QString &)));
    connect(ui->lineEdit_11, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_11(const QString &)));
    connect(ui->lineEdit_12, SIGNAL(textEdited(const QString&)), this, SLOT(handleChangeText_12(const QString &)));
    connect(ui->comboBox_4, SIGNAL(currentIndexChanged(int)), this, SLOT(handleChangeComboBox_4(int)));

}


void DialogManagePlanet::refreshListModel() {
    ui->listWidget->clear();
    ui->listWidget_3->clear();
    ListRenderer *list = glWindow->getManagerRenderer()->getAllList();
    list->iteratorFirst();
    QString id;
    while (list->iteratorHasNext()) {
        id = QString::fromStdString(list->iteratorNext()->id);
        ui->listWidget->addItem(new QListWidgetItem(id));
        ui->listWidget_3->addItem(new QListWidgetItem(id));
    }
}

void DialogManagePlanet::refreshListTexture() {
    ui->listWidget_2->clear();
    ui->listWidget_4->clear();
    List<string> list = glWindow->getManagerTexture()->getListID();
    list.iteratorFirst();
    QString id;
    while (list.iteratorHasNext()) {
        id = QString::fromStdString(list.iteratorNext());
        ui->listWidget_2->addItem(new QListWidgetItem(id));
        ui->listWidget_4->addItem(new QListWidgetItem(id));
    }
}

void DialogManagePlanet::getTexture2FromList(string id) {
    List<Texture*> list = glWindow->getManagerTexture()->list;
    list.iteratorFirst();
    QString id2;
    while (list.iteratorHasNext()) {
        Texture *texture = list.iteratorNext();
        if (texture->id == id) textureToApply = texture;
    }
}

void DialogManagePlanet::getTexture3FromList(string id) {
    List<Texture*> list = glWindow->getManagerTexture()->list;
    list.iteratorFirst();
    QString id2;
    while (list.iteratorHasNext()) {
        Texture *texture = list.iteratorNext();
        if (texture->id == id) textureToModify = texture;
    }
}

void DialogManagePlanet::messageBox(string t, string s){
    QMessageBox messageBox;
    messageBox.setWindowTitle(QString::fromStdString(t));
    messageBox.setText(QString::fromStdString(s));
    messageBox.setIcon(QMessageBox::Icon::Warning);
    messageBox.exec();
}

#define IS_KO -1
#define IS_OK 0
#define NAME_INVALID 1
#define NAME_ALWAYS_TAKEN 2
#define NO_TEXTURE 3
#define SIZE_INVALID 4
#define HIGH_INVALID 5
#define SPEED_INVALID 6
#define AXIS_INVALID 7
#define SUN_ENOUGH 8
#define SPEEDAXIS_INVALID 9

void DialogManagePlanet::verifIdPlanet(List<int> *list){
    string id = ui->lineEdit_2->text().toStdString();

    if(id.find(' ')!=-1) list->add(NAME_INVALID);
    else if(id=="") list->add(NAME_INVALID);
    if(glWindow->getManagerRenderer()->getAllList()->getROFromId(id)!=nullptr)
        list->add(NAME_ALWAYS_TAKEN);
    if(glWindow->getManagerRenderer()->getLightAvailable()==-1)
        list->add(SUN_ENOUGH);
}

void DialogManagePlanet::verifTexture(List<int> *list){
    if(ui->listWidget_2->currentItem()== nullptr) list->add(NO_TEXTURE);
}

void DialogManagePlanet::verifFloat(List<int> *list){
    if(ui->lineEdit->text()=="") list->add(SIZE_INVALID);
    if(ui->lineEdit_3->text()=="") list->add(HIGH_INVALID);
    if(ui->lineEdit_4->text()=="") list->add(SPEED_INVALID);
    if(ui->lineEdit_9->text()=="") list->add(AXIS_INVALID);
    if(ui->lineEdit_11->text()=="") list->add(SPEEDAXIS_INVALID);
}

void DialogManagePlanet::verifFloat_2(List<int> *list){
    if(ui->lineEdit_6->text()=="") list->add(SIZE_INVALID);
    if(ui->lineEdit_7->text()=="") list->add(HIGH_INVALID);
    if(ui->lineEdit_8->text()=="") list->add(SPEED_INVALID);
    if(ui->lineEdit_10->text()=="") list->add(AXIS_INVALID);
    if(ui->lineEdit_12->text()=="") list->add(SPEEDAXIS_INVALID);
}

string getMessageError(int code){
    switch(code){
        case NAME_INVALID: return "Name is invalid";
        case NAME_ALWAYS_TAKEN: return "Name is always taken";
        case NO_TEXTURE: return "No texture taken";
        case SIZE_INVALID: return "No size enter";
        case HIGH_INVALID: return "No high enter";
        case SPEED_INVALID: return "No speed enter";
        case AXIS_INVALID: return "No axis enter";
        case SUN_ENOUGH: return "Too much Sun";
        case SPEEDAXIS_INVALID: return "No speed axis enter";
        default: return "Error unknown";
    }
}

int DialogManagePlanet::verifAddPlanet(){
    List<int> list;
    verifIdPlanet(&list);
    verifTexture(&list);
    verifFloat(&list);
    if(list.isEmpty()){
        MessageBox messageBox("SUCCESS","Planet succesfully created ! :)",MessageBox::GREEN,MessageBox::INFO_ICON);
        return IS_OK;
    }else{
        string s="";
        list.iteratorFirst();
        while(list.iteratorHasNext()){
            if(s!="") s=s+"\n";
            s=s+"- "+getMessageError(list.iteratorNext());
        }
        MessageBox messageBox("ERROR",s,MessageBox::RED,MessageBox::WARNING_ICON);
        return IS_KO;
    }
}

void DialogManagePlanet::handleAddButton(){
    Log::debug("Add Model");
    if(verifAddPlanet()==IS_OK) {
        string id = ui->lineEdit_2->text().toStdString();
        Texture *texture = textureToApply;
        //bool *ok;
        string size = ui->lineEdit->text().toStdString();
        int unitSize = ui->comboBox->currentIndex();
        string high = ui->lineEdit_3->text().toStdString();
        int unitHigh = ui->comboBox_2->currentIndex();
        string speed = ui->lineEdit_4->text().toStdString();
        int unitSpeed = ui->comboBox_8->currentIndex();
        string axis = ui->lineEdit_9->text().toStdString();
        string speedA = ui->lineEdit_11->text().toStdString();
        int unitSpeedA = ui->comboBox_11->currentIndex();
        if(idComboBox==CPlanet){
            Planet *planet = new Planet(id, texture, size,unitSize,high,unitHigh,speed,unitSpeed,axis,
            speedA,unitSpeedA);
            glWindow->getManagerRenderer()->add(planet, true, true);
        }
        if(idComboBox==CSun){
            int co=glWindow->getManagerRenderer()->getLightAvailable();
            Sun *sun = new Sun(co,id,texture,size,unitSize,high,unitHigh,speed,unitSpeed,axis,
            speedA,unitSpeedA);
            glWindow->getManagerRenderer()->add(sun, true, true);
        }
        refreshListModel();
    }
}


int DialogManagePlanet::verifModifyPlanet(){
    List<int> list;
    verifFloat_2(&list);
    if(list.isEmpty()){
        MessageBox messageBox("SUCCESS","Planet succesfully modified ! :)",MessageBox::GREEN,MessageBox::INFO_ICON);
        return IS_OK;
    }else{
        string s="";
        list.iteratorFirst();
        while(list.iteratorHasNext()){
            if(s!="") s=s+"\n";
            s=s+"- "+getMessageError(list.iteratorNext());
        }
        MessageBox messageBox("ERROR",s,MessageBox::RED,MessageBox::WARNING_ICON);
        return IS_KO;
    }
}


void DialogManagePlanet::handleModifyButton(){
    Log::debug("Modify");
    if(verifModifyPlanet()==IS_OK) {
        objectToModify->texture=textureToModify;
        string size = ui->lineEdit_6->text().toStdString();
        int unitSize = ui->comboBox_7->currentIndex();
        objectToModify->changeSize(size,unitSize);
        string high = ui->lineEdit_7->text().toStdString();
        int unitHigh = ui->comboBox_6->currentIndex();
        objectToModify->changeHigh(high,unitHigh);
        string speed = ui->lineEdit_8->text().toStdString();
        int unitSpeed = ui->comboBox_10->currentIndex();
        objectToModify->changeSpeed(speed,unitSpeed);
        string axis = ui->lineEdit_10->text().toStdString();
        objectToModify->changeAxisTilt(axis);
        string speedA = ui->lineEdit_12->text().toStdString();
        int unitSpeedA = ui->comboBox_13->currentIndex();
        objectToModify->changeSpeedAxis(speedA,unitSpeedA);
        objectToModify->debug();
    }
}

void DialogManagePlanet::handleRemoveButton(){
    glWindow->getManagerRenderer()->remove(planetToRemove);
    refreshListModel();
    mainWindow->removeSwitchPlanet(planetToRemove);
    ui->pushButton->setEnabled(false);
}

void DialogManagePlanet::handleActivatedItem(QListWidgetItem *item) {
    ui->pushButton->setEnabled(true);
    planetToRemove=item->text().toStdString();
    glWindow->setLookPlanetId(item->text().toStdString());
}

void DialogManagePlanet::activateModify(RenderObject::Child child){
    ui->label_7->setEnabled(true);
    ui->label_8->setEnabled(true);
    ui->label_9->setEnabled(true);
    ui->label_10->setEnabled(true);
    ui->label_14->setEnabled(true);
    ui->label_15->setEnabled(true);
    ui->label_17->setEnabled(true);
    ui->lineEdit_5->setEnabled(true);
    ui->lineEdit_6->setEnabled(true);
    ui->lineEdit_7->setEnabled(true);
    ui->lineEdit_8->setEnabled(true);
    ui->lineEdit_10->setEnabled(true);
    ui->lineEdit_12->setEnabled(true);
    ui->listWidget_4->setEnabled(true);
    ui->comboBox_6->setEnabled(true);
    ui->comboBox_7->setEnabled(true);
    ui->comboBox_10->setEnabled(true);
    ui->comboBox_13->setEnabled(true);
}

void DialogManagePlanet::handleActivatedItem_3(QListWidgetItem * item){
    ui->pushButton_3->setEnabled(true);
    RenderObject *ro=glWindow->getManagerRenderer()->getAllList()->getROFromId(item->text().toStdString());
    objectToModify=ro;
    textureToModify=ro->texture;
    activateModify(ro->child);
    glWindow->setLookPlanetId(item->text().toStdString());
    ui->lineEdit_5->setText(QString::fromStdString(ro->id));
    ui->listWidget_4->setCurrentItem(ui->listWidget_4->getQListWidgetItem(ro->texture->id));
    ui->lineEdit_6->setText(QString::fromStdString(ro->sizeU));
    text_6=QString::fromStdString(ro->sizeU);
    ui->lineEdit_7->setText(QString::fromStdString(ro->highU));
    text_7=QString::fromStdString(ro->highU);
    ui->lineEdit_8->setText(QString::fromStdString(ro->speedU));
    text_8=QString::fromStdString(ro->speedU);
    ui->lineEdit_10->setText(QString::fromStdString(ro->axisU));
    text_10=QString::fromStdString(ro->axisU);
    ui->lineEdit_12->setText(QString::fromStdString(ro->speedAU));
    text_12=QString::fromStdString(ro->speedU);
    ui->comboBox_7->setCurrentIndex(ro->unitSize);
    ui->comboBox_6->setCurrentIndex(ro->unitHigh);
    ui->comboBox_10->setCurrentIndex(ro->unitSpeed);
    ui->comboBox_13->setCurrentIndex(ro->unitSpeedA);
    ui->toolBox_2->setCurrentIndex(1);
    //ui->lineEdit_6->setText(QString::fromStdString(stringro->size));
    //ui->lineEdit_7->setText(QString::fromStdString(ro->high));
    //ui->lineEdit_8->setText(QString::fromStdString(ro->id));
}

void DialogManagePlanet::handleActivatedItem2(QListWidgetItem *item) {
    getTexture2FromList(item->text().toStdString());
}

void DialogManagePlanet::handleActivatedItem3(QListWidgetItem *item) {
    getTexture3FromList(item->text().toStdString());
}
void DialogManagePlanet::handleChangeText_1(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit->setText(text_1);
    }else text_1=text;
}

void DialogManagePlanet::handleChangeText_3(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit_3->setText(text_3);
    }else text_3=text;
}

void DialogManagePlanet::handleChangeText_4(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit_4->setText(text_4);
    }else text_4=text;
}

void DialogManagePlanet::handleChangeText_6(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit_6->setText(text_6);
    }else text_6=text;
}

void DialogManagePlanet::handleChangeText_7(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit_7->setText(text_7);
    }else text_7=text;
}

void DialogManagePlanet::handleChangeText_8(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit_8->setText(text_8);
    }else text_8=text;
}

void DialogManagePlanet::handleChangeText_9(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit_9->setText(text_9);
    }else text_9=text;
}

void DialogManagePlanet::handleChangeText_10(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit_10->setText(text_10);
    }else text_10=text;
}

void DialogManagePlanet::handleChangeText_11(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit_11->setText(text_11);
    }else text_11=text;
}

void DialogManagePlanet::handleChangeText_12(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        ui->lineEdit_12->setText(text_12);
    }else text_12=text;
}

void DialogManagePlanet::handleChangeComboBox_4(int index){
    switch(index){
        case 0: idComboBox=CPlanet; break;
        case 1: idComboBox=CSun;break;
        default: break;
    }
}

DialogManagePlanet::~DialogManagePlanet() {
    delete ui;
}