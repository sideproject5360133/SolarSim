//
// Created by argo on 07/06/17.
//

#include "GLWidget.h"
#include "../Debug/Log.h"

GLWidget::GLWidget( int framesPerSecond, QWidget *parent, char *name)
        : QOpenGLWidget(parent) {
    setWindowTitle(name);
    //setMouseTracking(true);
    if (framesPerSecond == 0)
        t_Timer = NULL;
    else {
        int seconde = 1000; // 1 seconde = 1000 ms
        int timerInterval = seconde / framesPerSecond;
        t_Timer = new QTimer();
        t_Timer->setInterval(timerInterval);
        connect(t_Timer, SIGNAL(timeout()), this, SLOT(timeOutSlot()));
        t_Timer->start();
    }
}

void GLWidget::keyPressEvent(QKeyEvent *keyEvent) {
    Log::debug("Key Press Event");
}

void GLWidget::mouseMoveEvent(QMouseEvent *mouse) {
    GLint x = mouse->x();
    GLint y = mouse->y();
    camera.ProcessMouseMovementSphere(x - lastX, y - lastY);
    lastX = x;
    lastY = y;
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    Log::debug("Mouse Press Event");
    lastX=event->x();
    lastY=event->y();
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event) {
    Log::debug("Mouse Release Event");
}

void GLWidget::wheelEvent(QWheelEvent *event) {
    Log::debug("Mouse Wheel Event");
    camera.ProcessMouseScroll(event->delta()/120);
}

void GLWidget::timeOutSlot() {
    //TODO see problems
    update();
}