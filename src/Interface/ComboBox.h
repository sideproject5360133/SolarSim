//
// Created by argo on 15/08/17.
//

#ifndef SOLARSIM_COMBOBOX_H
#define SOLARSIM_COMBOBOX_H

#include <QMainWindow>
#include <QComboBox>
#include <src/Core/Unit.h>

class ComboBox : public QComboBox
{

public:
    explicit ComboBox(QWidget *parent = 0);


    int getIndex(std::string st);
    void add(std::string st);
    void remove(std::string st);
    void removeAll();

    Unit::Unit_Term getUnit(std::string st);
    Unit::Unit_Term getUnitIndex();
    Unit::Unit_Time getUnitIndex2();
    static Unit::Unit_Term getUnitIndex(int i);
    static Unit::Unit_Time getUnitIndex2(int i);

private:

protected slots:

};

#endif //SOLARSIM_COMBOBOX_H
