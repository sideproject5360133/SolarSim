//
// Created by argo on 24/08/17.
//

#include "LineEdit.h"
#include <src/Core/Verification.h>

LineEdit::LineEdit(QWidget *parent) :
        QLineEdit(parent)
{
    textFloat=QString("0.00");

}


void LineEdit::handleChangeText(const QString &text){
    if(!Verification::verifFloat(text.toStdString())){
        this->setText(textFloat);
    }else textFloat=text;
}