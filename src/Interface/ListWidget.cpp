//
// Created by argo on 17/08/17.
//

#include <QtWidgets/QListWidgetItem>
#include <src/Debug/Log.h>
#include "ListWidget.h"

ListWidget::ListWidget(QWidget *parent) :
        QListWidget(parent)
{


}

QListWidgetItem * ListWidget::getQListWidgetItem(std::string st){
    QSize size=this->size();
    int s=size.height();
    for(int i=0;i<s;i++){
        QListWidgetItem * qListWidgetItem=this->item(i);
        if(qListWidgetItem!= nullptr) {
            if(st==qListWidgetItem->text().toStdString()) return qListWidgetItem;
        }
    }
    return nullptr;

}