//
// Created by argo on 16/08/17.
//

#include <src/Core/String.h>
#include <QtWidgets/QAbstractButton>
#include "MessageBox.h"

MessageBox::MessageBox(string title, string message, Color color, Icon icon, bool exec) {
    messageBox.setWindowTitle(QString::fromStdString(title));
    setColor(message,color);
    setIcon(icon);
    if(exec) messageBox.exec();
    else{
        messageBox.addButton(QMessageBox::Ok);
        messageBox.addButton(QMessageBox::Cancel);
    }
}

bool MessageBox::exec(){
    QList<QAbstractButton *> list=messageBox.buttons();
    messageBox.exec();
    int i=list.size();
    for(int j=0;j<i;j++){
        QAbstractButton *abstractButton=list.at(j);
        if(messageBox.clickedButton()==abstractButton){
            if(abstractButton->text().toStdString()=="OK") return true;
        }
    }
    return false;
}

void MessageBox::addButton(QMessageBox::StandardButton standardButton){
    messageBox.addButton(standardButton);
}

void MessageBox::setIcon(Icon codeIcon){
    switch(codeIcon){
        case WARNING_ICON: messageBox.setIcon(QMessageBox::Icon::Warning); break;
        case INFO_ICON: messageBox.setIcon(QMessageBox::Icon::Information); break;
        default: break;
    }
}


void MessageBox::setColor(string message,Color color){
    switch(color){
        case GREEN: message=String::colorize(message,String::GREEN);
        case RED: message=String::colorize(message,String::RED);
        default: break;
    }
    messageBox.setText(QString::fromStdString(message));
}